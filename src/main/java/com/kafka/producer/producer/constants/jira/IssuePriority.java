package com.kafka.producer.producer.constants.jira;

public enum IssuePriority {
    CRITICAL,
    HIGH,
    MEDIUM,
    LOW
}

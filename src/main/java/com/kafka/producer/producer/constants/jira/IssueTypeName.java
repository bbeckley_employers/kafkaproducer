package com.kafka.producer.producer.constants.jira;

public enum IssueTypeName {
    BUG,
    INCIDENT;
}

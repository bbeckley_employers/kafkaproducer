package com.kafka.producer.producer.rest;

import com.employers.inc.jiraservice.JiraCreateIssueAvro;
import com.kafka.producer.producer.kafka.ProducerExample;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestResource {

    private ProducerExample producerExample;

    public TestResource(ProducerExample producerExample) {
        this.producerExample = producerExample;
    }

    int i = 0;

    @RequestMapping(path = "publish/issue", method = RequestMethod.GET)
    public void publishIssue() {

        JiraCreateIssueAvro avroIssue = producerExample.createAvroIssue(i);
        producerExample.publishJiraIssue("notSure", avroIssue);
        i++;
    }
}

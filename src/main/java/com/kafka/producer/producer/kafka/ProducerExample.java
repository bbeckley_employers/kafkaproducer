package com.kafka.producer.producer.kafka;

import com.employers.inc.jiraservice.JiraCreateIssueAvro;
import com.kafka.producer.producer.constants.jira.IssuePriority;
import com.kafka.producer.producer.constants.jira.IssueTypeName;
import io.confluent.examples.clients.cloud.DataRecordAvro;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.IntStream.range;


@Log4j2
@Component
@RequiredArgsConstructor
public class ProducerExample {
    @Value("${com.employers.inc.config.topic.name}")
    private String topicNameDataRecord;

    @Value("${com.employers.inc.config.topic.name.jira.issue}")
    private String topicNameJiraIssue;

    private final KafkaTemplate<String, DataRecordAvro> producerDataRecord;

    private final KafkaTemplate<String, JiraCreateIssueAvro> producerJiraIssue;

    @EventListener(ApplicationStartedEvent.class)
    public void produce() {
        // Produce sample data
        StopWatch sw = new StopWatch("Kafka");

        int numberOfRecords = 10;

        sw.start("looping");
        range(0, numberOfRecords).parallel().forEach(i -> {
            final String key = "alice";
            final DataRecordAvro record = new DataRecordAvro((long) i);
//          log.info("Producing record: {}\t{}", key, record);

            producerDataRecord.send(topicNameDataRecord, key, record).addCallback(
                    result -> {
                        final RecordMetadata m;
                        if (result != null) {
                            m = result.getRecordMetadata();
                            log.info("Produced record to topic {} partition {} @ offset {}",
                                    m.topic(),
                                    m.partition(),
                                    m.offset());
                        }
                    },
                    exception -> log.error("Failed to produce to kafka", exception));
        });

        producerDataRecord.flush();

        sw.stop();

        System.out.println(sw.prettyPrint());
        System.out.println(sw.getTotalTimeSeconds());

        log.info(numberOfRecords + " messages were produced to topic {}", topicNameDataRecord);

    }

//    @EventListener(ApplicationStartedEvent.class)
//    public void produceJira() {
//        // Produce sample data
//        range(0, 1).forEach(i -> {
//            final String key = "alice";
//
//            JiraCreateIssueAvro issueAvro = createAvroIssue(i);
//
//            publishJiraIssue(key, issueAvro);
//        });
//    }

    public JiraCreateIssueAvro createAvroIssue(int i) {
        final String key = "alice";
        List<String> messages = new ArrayList<>();
        String msg1 = "Policy Number: EIG " + i + i + i + i + i;
        String msg2 = "Policy Name: Company " + i;
        String msg3 = "PrecisePay payment issue $" + i;

        messages.add(msg1);
        messages.add(msg2);
        messages.add(msg3);

//        JiraCreateIssueAvro build = new JiraCreateIssueAvro().newBuilder().setSummary("test").setIssueType("test2").setPriority("aaa").setProjectId("ces1").setStackTrace("here").build();

        JiraCreateIssueAvro issueAvro = new JiraCreateIssueAvro("", "", "", "", "", null, null, "DEV");
        issueAvro.setIssueType(IssueTypeName.INCIDENT.name());
        issueAvro.setMessages(messages);
        issueAvro.setPriority(IssuePriority.CRITICAL.name());
        issueAvro.setProjectId("CES1");
        issueAvro.setSummary("Kafka Test " + i);
        issueAvro.setLabels(Arrays.asList("AutomatedServiceDesk"));

        try {
            String test = null;
            test.toLowerCase();
        } catch (Exception e) {
            String stacktrace = ExceptionUtils.getStackTrace(e);
            issueAvro.setStackTrace(stacktrace);
        }

        return issueAvro;
    }

    public void publishJiraIssue(String key, JiraCreateIssueAvro issueAvro) {
        producerJiraIssue.send(topicNameJiraIssue, key, issueAvro).addCallback(
                result -> {
                    final RecordMetadata m;
                    if (result != null) {
                        m = result.getRecordMetadata();
                        log.info("Produced record to topic {} partition {} @ offset {}",
                                m.topic(),
                                m.partition(),
                                m.offset());
                    }
                },
                exception -> log.error("Failed to produce to kafka", exception));

        producerJiraIssue.flush();

        log.info("Message was produced to topic {}", topicNameJiraIssue);
    }

}
